use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fmt;
use std::path::Path;
use std::process::Command;
use std::time::Duration;

const REPO_URL: &str = "https://github.com/rust-lang/crates.io-index";
const CRATE_URL: &str = "https://static.crates.io/crates/{name}/{name}-{version}.crate";
const CRATE_FILE: &str = "{name}-{version}.crate";

type Res = Result<(), Box<dyn Error>>;

#[derive(Debug, Clone)]
pub struct ExecError(String);

impl Error for ExecError {}

impl ExecError {
    pub fn new<P: Display>(msg: P) -> Self {
        Self(msg.to_string())
    }
}

impl Display for ExecError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Encountered error: {}", self.0)
    }
}


pub fn new_err<P: Display>(msg: P) -> Res {
    return Err(Box::new(ExecError(msg.to_string())));
}


fn clone_or_update_repo(path: &Path) -> Res {
    let output = if !path.exists() {
        Command::new("git")
            .arg("clone")
            .arg(REPO_URL)
            .arg(path.to_string_lossy().to_string())
            .output()?
    } else {
        Command::new("git")
            .arg("pull")
            .current_dir(path)
            .output()?
    };

    let stdout = String::from_utf8_lossy(&output.stdout);
    let err = String::from_utf8_lossy(&output.stderr);
    println!("{}", stdout);
    eprintln!("{}", err);

    if !output.status.success() {
        return new_err("Status de sortie de Git incorrect !");
    }

    Ok(())
}

#[derive(serde::Deserialize)]
struct IndexEntry {
    name: String,
    vers: String,
}

fn process_index(path: &Path, packages_location: &Path) -> Res {
    println!("Process package {}", path.file_name().unwrap().to_string_lossy());

    let file = std::fs::read_to_string(path)?;

    let pkg_loc = packages_location.join(path.file_name().unwrap());
    if !pkg_loc.is_dir() {
        std::fs::create_dir_all(&pkg_loc)?;
    }

    for line in file.split("\n") {
        if line.is_empty() {
            continue;
        }

        let entry: IndexEntry = serde_json::from_str(line)?;
        let url = CRATE_URL
            .replace("{name}", &entry.name)
            .replace("{version}", &entry.vers);

        let file = CRATE_FILE
            .replace("{name}", &entry.name)
            .replace("{version}", &entry.vers);
        let file = pkg_loc.join(file);

        if file.exists() {
            continue;
        }

        println!("Download {}", url);
        let res = reqwest::blocking::Client::default().get(url)
            .timeout(Duration::from_secs(180))
            .send()?;
        if !res.status().is_success() {
            eprintln!("Failed to download package with status {} !", res.status());
        } else {
            let package = res.bytes()?;
            std::fs::write(file, package)?;
        }
    }

    Ok(())
}

fn recurse_scan(scan_path: &Path, packages_location: &Path) -> Res {
    for entry in std::fs::read_dir(scan_path)? {
        let entry = entry?;
        let path = entry.path();

        if path.is_dir() && !path.file_name().unwrap().eq(".git")
            && !path.file_name().unwrap().eq(".github") {
            recurse_scan(&path, packages_location)?;
        } else if path.is_file() && !path.file_name().unwrap().eq("config.json") {
            process_index(&path, packages_location)?;
        }
    }

    Ok(())
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();

    if args.len() != 2 {
        eprintln!("Usage: {} [storage-path]", args[0]);
        return;
    }

    let storage = Path::new(&args[1]);
    let git_path = storage.join("git-repo");
    let package_location = storage.join("packages");

    clone_or_update_repo(&git_path).expect("Failed to clone repo !");

    recurse_scan(&git_path, &package_location).expect("Failed to recurse scan !");
}
